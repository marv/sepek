# Copyright 2009 Jan Meier
# Copyright 2010 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require launchpad [ branch="$(ever range 1-2)-series" ]
require setup-py [ import='setuptools' blacklist='3' has_bin=true ]

SUMMARY="Encrypted bandwidth-efficient backup using the rsync algorithm"
DESCRIPTION="
Duplicity backs up directories by producing encrypted Tar-format volumes and
uploading them to a remote or local file server. Because duplicity uses
librsync, the incremental archives are space efficient and only record the
parts of files that have changed since the last backup. Because duplicity uses
GnuPG to encrypt and/or sign these archives, they will be safe from spying
and/or modification by the server.

This package also includes the rdiffdir utility, an extension of librsync's
rdiff to directories. Rdiffdir can be used to produce signatures and deltas in
GNU Tar format of directories as well as regular files.
"
HOMEPAGE="http://duplicity.nongnu.org/"

LICENCES="GPL-2 LGPL-2.1 MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        net-libs/librsync[>=0.9.6]
    test:
        dev-python/mock[python_abis:*(-)?]
    run+test:
        dev-python/lockfile[>=0.9][python_abis:*(-)?]
    suggestion:
        app-crypt/gnupg[>=1.0] [[ description = [ Needed to enrypt your backups ] ]]
        dev-python/boto[>=2.1.1][python_abis:*(-)?] [[ description = [ Needed to uploud your backups to S3 ] ]]
        dev-python/paramiko[python_abis:*(-)?] [[ description = [ Needed to uploud your backups to SSH servers ] ]]
        dev-python/python-swiftclient[python_abis:*(-)?] [[ description = [ Needed to uploud your backups to the OpenStack Cloud ] ]]
        net-ftp/lftp[>=3.7.15] [[ description = [ Needed to uploud your backups to FTPS servers ] ]]
        net-ftp/ncftp[>3.2.0] [[ description = [ Needed to uploud your backups to FTP servers ] ]]
"

# GPG tests fail most likely because additional setup steps for testing are required
RESTRICT="test"

src_install() {
    setup-py_src_install
    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/{COPYING,tarfile-LICENSE}
}

